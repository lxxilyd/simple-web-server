package top.lixxing.web.server.utils;

import java.io.InputStream;

public class CommandExec {

	/**
	 * 执行系统命令
	 * @param command
	 * @return 执行结果
	 */
	public static String exec(String command) {
		InputStream inputStream = null;
		InputStream errorStream = null;
		try {
			ProcessBuilder processBuilder = new ProcessBuilder();
			processBuilder.command(command);
			Process start = processBuilder.start();
			inputStream = start.getInputStream();
			errorStream = start.getErrorStream();
			byte[] normalData = IOUtils.readFromStream(inputStream);
			byte[] errorData = IOUtils.readFromStream(errorStream);
			String charSet = System.getProperties().getProperty("sun.jnu.encoding");
			return normalData.length != 0 ? new String(normalData, charSet) : new String(errorData, charSet);
		} catch (Exception e) {
			e.printStackTrace();
			return e.getMessage();
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (errorStream != null) {
				try {
					errorStream.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
}
